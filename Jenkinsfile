pipeline {

    agent any

    tools {
      maven 'MAVEN3'
      jdk 'JDK11'
    }

    environment {
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "172.17.0.3:8081/"
        NEXUS_REPOSITORY = "maven-nexus-repo"
        NEXUS_CREDENTIAL_ID = "nexus-user-credentials"
        
    }

    stages {
        stage ('clone'){
            steps{
            git "https://gitlab.com/HarounMeg/spring-petclinic-fitec.git"
            }
        }

        stage('SonarQube analysis') {
            steps{
                withSonarQubeEnv('sonarqube') {
                sh "mvn clean package sonar:sonar"
                } 
            } // submitted SonarQube taskId is automatically attached to the pipeline context
        }

        stage('build') {
           
                steps{
                sh "mvn clean install"
                } 
           
        }

        stage("Publish to Nexus Repository Manager") {
            steps {
                script {
                    pom = readMavenPom file: "pom.xml";
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    artifactPath = filesByGlob[0].path;
                    artifactExists = fileExists artifactPath;
                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";
                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            ]
                        );
                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
        }

        stage('execute Performance Tests') {
            steps{
            sh '/etc/apache-jmeter-5.4.1/bin/jmeter.sh -n -t /home/harm/spring-petclinic-fitec/petclinic_test_plan.jmx -l test.jml'
            }
        
        }

        stage('Building Image petclinic et mysql') {
            steps{
                sh 'docker build -t petclinic .'
                sh 'docker-compose up -d'
            }
        }
        stage('Push MySQL Image') {
            steps{
        withCredentials([azureServicePrincipal('azure connexion')]){
        sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
        sh 'az account set -s $AZURE_SUBSCRIPTION_ID'
        sh 'az acr login -n petclinichls'
        //sh 'docker build -t petclinic .'
        sh 'docker tag  mysql:5.7 petclinichls.azurecr.io/mysql:5.7'
        sh 'docker push petclinichls.azurecr.io/mysql:5.7'
                }
            }
        }

        stage('Push Petclinic Image') {
            steps{
        withCredentials([azureServicePrincipal('azure connexion')]){
        sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
        sh 'az account set -s $AZURE_SUBSCRIPTION_ID'
        sh 'az acr login -n petclinichls'
        //sh 'docker build -t petclinic .'
        sh 'docker tag  petclinic petclinichls.azurecr.io/petclinic:latest'
        sh 'docker push petclinichls.azurecr.io/petclinic:latest'
                }
            }
        }
        stage('Select AKS cluster') {
            steps{  
        withCredentials([azureServicePrincipal('azure connexion')]){
        sh 'docker login -u ${AZURE_CLIENT_ID} -p ${AZURE_CLIENT_SECRET} petclinichls.azurecr.io'
        sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
        sh 'az account set -s $AZURE_SUBSCRIPTION_ID'
        sh 'az account set --subscription $AZURE_SUBSCRIPTION_ID'
        sh 'kubectl apply -f /var/lib/jenkins/workspace/Pet-Clinic/2petclinic.yml'
        sh 'kubectl get deployments --all-namespaces=true'      
        sh 'kubectl get pods --all-namespaces=true'     
        sh 'kubectl get service --all-namespaces=true' 
                }
            }
        }
    }
}
